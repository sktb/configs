export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_TIME=en_US.UTF-8
export LC_MESSAGES=en_US.UTF-8
export LC_ALL=$LANG

case "${OSTYPE}" in
darwin*)
  alias ls='ls -aliF -G'
  ;;
linux*)
  alias ls='ls -aliF --color=auto'
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init -)"
  ;;
esac
