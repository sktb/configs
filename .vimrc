"----------------------------------------
" Plugins (init Vundle)
"----------------------------------------
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

" 導入したいプラグインを以下に列挙
" Plugin '[Github Author]/[Github repo]' の形式で記入
Plugin 'VundleVim/Vundle.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'preservim/nerdtree'
Plugin 'bronson/vim-trailing-whitespace'

call vundle#end()
filetype plugin indent on

"　その他のカスタム設定を以下に書く


"----------------------------------------
" 検索
"----------------------------------------
" 検索するときに大文字小文字を区別しない
set ignorecase
" 小文字で検索すると大文字と小文字を無視して検索
set smartcase
" 検索がファイル末尾まで進んだら、ファイル先頭から再び検索
set wrapscan
" インクリメンタル検索 (検索ワードの最初の文字を入力した時点で検索が開始)
set incsearch
" 検索結果をハイライト表示
set hlsearch

"----------------------------------------
" 編集系
"----------------------------------------
" UTF-8を使え
set fileencoding=utf-8
set encoding=utf-8
" 自動読み込み
set autoread
" Syntax highlight
syntax on
" コピペ
set paste
" タブ文字の表示幅
set tabstop=4
" スペースインデント
set expandtab
" 半角スペースの数
set shiftwidth=2
" 改行時の自動インデント
set smartindent
" バックスペースを、空白、行末、行頭でも
set backspace=indent,eol,start
" 矩形選択をいい感じに
set virtualedit=block
" 折返し有効
set wrap

"----------------------------------------
" 表示系
"----------------------------------------
set whichwrap=h,l
" 行番号表示
set number
set cursorline

" タイトル表示
set title

" タブインデントを表示
set list
set listchars=tab:>-

" 全角ﾀﾋね
highlight zenkakuda ctermbg=7
call matchadd("zenkakuda", '\%u3000')

"----------------------------------------
" 表示系 (ウィンドウの下)
"----------------------------------------
" show cmd
set showcmd
" enable status line
set laststatus=2
" display on status line
" index.html                                                                         [dos][encode:utf-8][1/2471,1,Top]
set statusline=%y\ %f\ %m%r%h%w%=[%{&ff}][encode:%{&fileencoding}][%l/%L,%c%V,%P]

"----------------------------------------
" Plugin Settings
"----------------------------------------
" vimを立ち上げたときに、自動的にvim-indent-guidesをオンにする
let g:indent_guides_enable_on_vim_startup = 1

" フォルダアイコンを表示
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Nerd\ Font\ Complete\ 12
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""
let g:WebDevIconsUnicodeDecorateFolderNodes = v:true
" after a re-source, fix syntax matching issues (concealing brackets):
if exists('g:loaded_webdevicons')
call webdevicons#refresh()
endif


"----------------------------------------
" Key map
"----------------------------------------
nnoremap <silent><C-E> :NERDTreeToggle<CR>
